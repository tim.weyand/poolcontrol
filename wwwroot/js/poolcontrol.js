var poolcontrol = {
    dataURL: 'GetState.json',
    dataInterval: 1000,
    activeInterval: null,
    currentData: null,
    domData:{
        sysinfo:{},
        sensors:{},
        actors:{}
    },

    getData: function() {
        let xhr = new XMLHttpRequest();
        let dataURL = poolcontrol.dataURL;
        if (poolcontrol.currentData != null) {
            dataURL+="?position="+poolcontrol.currentData.id
        }
        xhr.open('GET', dataURL);
        xhr.onload = function() {
            if (xhr.status === 200) {
                poolcontrol.processData(JSON.parse(xhr.responseText));
            } else if (xhr.status !== 304) {
                console.log('Request failed');
            }
        }
        return xhr.send();
    },

    processData: function(newData) {
        if (poolcontrol.currentData!=null) {
            let timeelapsed = newData.time-poolcontrol.currentData.time;
            if ((timeelapsed/poolcontrol.dataInterval)>2) {
                poolcontrol.stop();
                let newInterval = Math.round(timeelapsed/1.95);
                console.log("Poolcontrol: Resync Data-Retrival Interval ("+poolcontrol.dataInterval+"ms =>"+newInterval+"ms)")
                poolcontrol.dataInterval=newInterval;
                poolcontrol.start();
            }
        }
        poolcontrol.currentData = newData;
        poolcontrol.changePlaceholder(newData);

    },

    convertName: function(string) {
        return string.toLowerCase().replace(/[^a-zA-Z0-9\+\-]+/g, "_");
    },

    scanForPlaceholderDomElements: function() {
        let elements = document.getElementsByClassName('poolControlData');

        for (let prop in elements) {
            if (typeof elements[prop] != 'object' && typeof elements[prop].getAttribute == 'undefined') continue;
            let ident = elements[prop].getAttribute('data-name').split('.');

            poolcontrol.domData[ident[0]][ident[1]] = elements[prop];
        }
    },

    changePlaceholder: function(data) {
        for (let prop in data.sysinfo) {
            let value   = data.sysinfo[prop];
            let current = poolcontrol.domData.sysinfo[this.convertName(prop)];
            if (typeof current != "undefined") {
                current.setAttribute("data-value",value);
                current.innerHTML = value;
            }
        }

        let removeChanged = document.getElementsByClassName("changed");
        for (let prop in removeChanged) {
            if (typeof removeChanged[prop]=="object") {
                removeChanged[prop].classList.remove("changed","increased","decreased");
            }
        }

        for (let prop in data.sensors) {
            let value   = data.sensors[prop];
            let current = poolcontrol.domData.sensors[this.convertName(value['name'])];
            if (typeof current != "undefined") {
                if (current.getAttribute('data-value') != value['value']) {
                    let current_value = current.getAttribute('data-value');
                    current.classList.add("changed");

                    current.setAttribute("data-value",value['value']);
                    console.log(value['value'],value['maxValue']);
                    current.setAttribute("data-max",value['maxValue']);
                    current.setAttribute("data-min",value['minValue']);
                    current.setAttribute("data-unit",value['unit']);
                    if (value['unit']=="C") {
                        current.innerHTML = value['value']+' °C';
                    } else {
                        current.innerHTML = value['value']+' '+value['unit'];
                    }

                    if (current_value > value['value']) {
                        current.classList.add("decreased");
                        current.classList.remove("increased");
                    }
                    if (current_value < value['value']) {
                        current.classList.add("increased");
                        current.classList.remove("decreased");
                    }
                } else {
                    current.classList.remove("changed");
                    current.classList.remove("increased");
                    current.classList.remove("decreased");
                }
            }
        }

        for (let prop in data.actors) {
            let value   = data.actors[prop];
            let current = poolcontrol.domData.actors[this.convertName(value['name'])];

            if (typeof current != "undefined") {
                if (current.getAttribute('data-value') != value['status']) {
                    current.classList.add("changed");

                    if (current.getAttribute('data-value') > value['status']) {
                        current.classList.add("decreased");
                        current.classList.remove("increased");
                    }
                    if (current.getAttribute('data-value') < value['status']) {
                        current.classList.add("increased");
                        current.classList.remove("decreased");
                    }
                } else {
                    current.classList.remove("changed");
                    current.classList.add("increased");
                    current.classList.remove("decreased");
                }

                current.setAttribute("data-value",value['status']);
                current.setAttribute("data-status",value['statusDescription']);
                current.innerHTML = value['status'];
            }
        }
    },

    startDebugDemo: function() {
        console.log("Starting Demo")
        let xhr = new XMLHttpRequest();
        xhr.open('GET', poolcontrol.dataURL);
        xhr.onload = function() {
            console.log("Got Data");
            console.log("Status: "+xhr.status);
            if (xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                console.log(data);

                let html = '<h1>GoLang PoolDigital ProCon Proxy Debug</h1><div><h2>Sysinfo</h2>';
                html += '<table><tr><th style="width:150px">Name</th><th style="width:200px">Value</th><th>HTML Placeholder</th></tr>';

                for (let prop in data.sysinfo) {
                    let placeholder_name = 'sysinfo.' + poolcontrol.convertName(prop);
                    html += '<tr><td>' + prop + '</td><td><span class="poolControlData" data-name="' + placeholder_name + '"></span></td><td><code>&#x3C;span class=&#x22;poolControlData&#x22; data-name=&#x22;' + placeholder_name + '&#x22;&#x3E;&#x3C;/span&#x3E;</code></td></tr>';
                }

                html += '</table></div><div><h2>SensorData</h2>';
                html += '<table><tr><th style="width:150px">Name</th><th style="width:200px">Value</th><th>HTML Placeholder</th></tr>';

                for (let prop in data.sensors) {
                    let placeholder_name = 'sensors.' + poolcontrol.convertName(data.sensors[prop]['name']);
                    html += '<tr><td>' + data.sensors[prop]['name'] + '</td><td><span class="poolControlData" data-name="' + placeholder_name + '"></span><td><code>&#x3C;span class=&#x22;poolControlData&#x22; data-name=&#x22;' + placeholder_name + '&#x22;&#x3E;&#x3C;/span&#x3E;</code></td>';
                }

                html += '</table></div><div><h2>ActorsData</h2>';

                html += '<table><tr><th style="width:150px">Name</th><th style="width:200px">Value</th><th>HTML Placeholder</th></tr>';
                for (let prop in data.actors) {
                    let placeholder_name = 'actors.' + poolcontrol.convertName(data.actors[prop]['name']);
                    html += '<tr><td>' + data.actors[prop]['name'] + '</td><td><span class="poolControlData" data-name="' + placeholder_name + '"></span></td><td><code>&#x3C;span class=&#x22;poolControlData&#x22; data-name=&#x22;' + placeholder_name + '&#x22;&#x3E;&#x3C;/span&#x3E;</code></td></tr>';
                }
                html += '</table></div>';
                document.getElementById('PoolControl2019debugData').innerHTML = html;
                poolcontrol.scanForPlaceholderDomElements();
            }
        };
        xhr.send();
    },

    start: function() {
       poolcontrol.activeInterval = window.setInterval(poolcontrol.getData,poolcontrol.dataInterval);
    },
    stop: function() {
       window.clearInterval(poolcontrol.activeInterval);
    },

    bootup: function() {
        console.log("Bootup poolcontrol.js");
        console.log("dataURL: "+poolcontrol.dataURL+" (Interval "+poolcontrol.dataInterval+"ms)");

        if (document.getElementById('PoolControl2019debugData')) {
            poolcontrol.startDebugDemo();
        }

        poolcontrol.scanForPlaceholderDomElements();
        poolcontrol.start();
    },
}

//Setting Variables from currentScript
if (typeof document.currentScript!="undefined") {
    if (document.currentScript.getAttribute("data-src")) {
        poolcontrol.dataURL = document.currentScript.getAttribute("data-src");
    }

    if (document.currentScript.getAttribute("data-interval") && parseInt(document.currentScript.getAttribute("data-interval")) > 500) {
        poolcontrol.dataInterval = parseInt(document.currentScript.getAttribute("data-interval"),10);
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    poolcontrol.bootup();
});
