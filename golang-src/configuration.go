package main

import (
	"log"
	"os"
	"strconv"
	"time"
)


var configuration configurationStruct

func configInit() {
	if os.Getenv("enablePulling")=="" || os.Getenv("enablePulling")=="true" {
		configuration.enablePulling = true
	} else {
			configuration.enablePulling = false
	}

	if os.Getenv("getStateURL")!="" {
		configuration.getStateURL = os.Getenv("getStateURL")
	} else {
		configuration.getStateURL = "https://pool.weyand.biz/GetState.csv"
	}

	if os.Getenv("pushStateURL")!="" {
		configuration.pushStateURL = os.Getenv("pushStateURL")
	}

	if os.Getenv("pullIntervall")!="" {
		value, err := strconv.ParseInt(os.Getenv("pullIntervall"), 10, 64)
		if err != nil {
			configuration.pullIntervall=5
		} else {
			configuration.pullIntervall = time.Duration(value)
		}
	} else {
		configuration.pullIntervall=5
	}

	if os.Getenv("removeNAnames")=="" || os.Getenv("removeNAnames")=="true" {
		configuration.removeNAnames = true
	} else {
		configuration.removeNAnames = false
	}

	if os.Getenv("enableWebserver")=="" || os.Getenv("enableWebserver")=="true" {
		configuration.enableWebserver = true
	} else {
		configuration.enableWebserver = false
	}

	if os.Getenv("allowHTTPUpdate")=="" || os.Getenv("allowHTTPUpdate")=="true" {
		configuration.allowHttpUpdate = true
	} else {
		configuration.allowHttpUpdate = false
	}

	if os.Getenv("ShareSecret")!="" {
		configuration.shareSecret = os.Getenv("ShareSecret")
	} else {
		configuration.shareSecret = "IamASecret129$"
	}

	if os.Getenv("webserverBase")!="" {
        configuration.webserverBase = os.Getenv("webserverBase")
	} else {
		configuration.webserverBase = ""
	}

	if os.Getenv("CURRENT_ROOT") != "" {
		configuration.webserverRoot = os.Getenv("CURRENT_ROOT")
	} else {
		configuration.webserverRoot = "wwwroot"
	}

	log.Println("current WebserverBase: "+configuration.webserverBase)
	log.Println("current WebserverRoot: "+configuration.webserverRoot)
}
