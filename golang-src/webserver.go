package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/NYTimes/gziphandler"
	"log"
	"net/http"
	"strconv"
)

func webGetStateCSV(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Header().Add("Content-Type", "text/csv; charset=utf-8")
	w.Header().Add("Cache-Control", "public, max-age:10")
	w.Header().Add("Access-Control-Allow-Origin: ", "*")
	fmt.Fprintf(w, "%s", getStateData.OriginalData)
}

func webGetStateJSON(w http.ResponseWriter, r *http.Request) {
	position, ok := r.URL.Query()["position"]
	returnData:= getStateData

	if ok && len(position)==1 {
		currentPosition,_:= strconv.ParseInt(position[0], 10, 64)
		if currentPosition == getStateData.Id {
			w.WriteHeader(304)
			fmt.Fprintf(w, "")
			return
		} else {
			returnData=getChanges(currentPosition)
		}
	}

	b, err := json.Marshal(returnData)
	if err != nil {
		fmt.Println(err)
		log.Println(err)
		return
	}
	w.WriteHeader(200)
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Header().Add("Access-Control-Allow-Origin: ", "*")
	w.Header().Add("Cache-Control", "public, max-age:5")
	fmt.Fprintf(w, "%s", string(b))
}

func webUpdateState(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" || configuration.allowHttpUpdate!=true {
		w.WriteHeader(403)
		w.Header().Add("Cache-Control", "private")
		w.Header().Add("Content-Type", "plain/text; charset=utf-8")
		fmt.Fprintf(w, "%s", "Not Allowed")
		return
	}

	err := r.ParseForm()
	if err != nil {
		panic(err)
	}
	secret := r.Form.Get("secret")
	if secret != configuration.shareSecret {
		w.WriteHeader(403)
		w.Header().Add("Cache-Control", "private")
		w.Header().Add("Content-Type", "plain/text; charset=utf-8")
		fmt.Fprintf(w, "%s", "Not Allowed")
		return
	}

	tmpData, err := base64.StdEncoding.DecodeString(r.Form.Get("data"))
	if err != nil {
		log.Fatal("error:", err)
	}

	data:= string(tmpData)

	w.Header().Add("Cache-Control", "private")
	if (len(data)<=10) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "%s", "failed")
	} else {
		w.WriteHeader(200)
		getStateConvert(data)
		fmt.Fprintf(w, "%s", "ok")
	}
}


func startWebserver() {
	log.Println("Initializing Poolcontrol Webserver")

	webPlainGetStateCSV := http.HandlerFunc(webGetStateCSV)
	webGzGetStateCSV    := gziphandler.GzipHandler(webPlainGetStateCSV)
	http.Handle(configuration.webserverBase+"/GetState.csv", webGzGetStateCSV)

	webPlainGetStateJSON := http.HandlerFunc(webGetStateJSON)
	webGzGetStateJSON    := gziphandler.GzipHandler(webPlainGetStateJSON)
	http.Handle(configuration.webserverBase+"/GetState.json", webGzGetStateJSON)


	fileserver           := http.FileServer(FileSystem{http.Dir(configuration.webserverRoot)})
	fileserverGz         := gziphandler.GzipHandler(fileserver)
	http.Handle(configuration.webserverBase+"/", fileserverGz)


	http.HandleFunc(configuration.webserverBase+"/UpdateState", webUpdateState)

	http.ListenAndServe(":80", nil)
}