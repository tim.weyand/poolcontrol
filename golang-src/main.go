package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var getStateDataHistory []getStateJSON
var getStateData getStateJSON

func saveGetStateHistory(currentData getStateJSON) {
	newData:=[]getStateJSON{currentData}
	for key, value := range getStateDataHistory {
		if (key<3600) {
			newData=append(newData,value)
		}
	}
	getStateDataHistory = newData
}

func getChanges(position int64) getStateJSON {
	currentPosition:=getStateData.Id
	if currentPosition>position && (currentPosition-position)<3599 {
		if getStateDataHistory[(currentPosition-position)].Id==position {
			oldData:=getStateDataHistory[(currentPosition-position)]
			deltaData:=getStateJSON{}
			deltaData.Id = currentPosition
			deltaData.DataTime = getStateData.DataTime
			deltaData.Sysinfo  = getStateData.Sysinfo

			for key, value := range getStateData.Actors {
				if value!=oldData.Actors[key] {
					deltaData.Actors=append(deltaData.Actors, value)
				}
			}

			for key, value := range getStateData.Sensors {
				if value!=oldData.Sensors[key] {
					deltaData.Sensors=append(deltaData.Sensors, value)
				}
			}

			return deltaData
		}
	}
	return getStateData
}

func getStateCSV() string {
	tr := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   1 * time.Second,
			KeepAlive: 1 * time.Second,
			DualStack: false,
		}).DialContext,
		MaxIdleConns:       1,
		IdleConnTimeout:    2 * time.Second,
	}
	client   := &http.Client{CheckRedirect: func(req *http.Request, via []*http.Request) error {return http.ErrUseLastResponse},Transport: tr}
	req, err := http.NewRequest("GET", configuration.getStateURL, nil)
	req.Header.Add("ACCEPT-ENCODING", "br, gzip, deflate")
	if err != nil {
		// handle error
		log.Print(err)
		return "-1"
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return "-1"
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != 200 {
		log.Print(err)
		return "-1"
	}

    return string(body)
}

func getStateConvert(tmpGetStateCSV string) {
	if tmpGetStateCSV=="-1" || len(tmpGetStateCSV)<20 {
		log.Print("Error with Source Data")
		return
	}

	cleanedLines        := strings.Split(strings.Replace(tmpGetStateCSV, "\r\n", "\n", -1),"\n")
	returnValue         := getStateJSON{}
	returnValue.DataTime = time.Now().UnixNano() / int64(time.Millisecond)

	if getStateData.Id!=0 {
		returnValue.Id=getStateData.Id+1
	} else {
		returnValue.Id=1
	}

	returnValue.OriginalData = tmpGetStateCSV
	// sort some data
	var tempArray [100][10]string
	for key, value := range cleanedLines {
		if key == 0 {
			returnValue.Sysinfo.Info = value
			currentLine:= strings.Split(value, ",")
			RawUptime,_:= strconv.Atoi(currentLine[2])
			returnValue.Sysinfo.Uptime = RawUptime
		} else if key < len(cleanedLines) {
			currentLine:= strings.Split(value, ",")
			for key2, value2 := range currentLine {
				tempArray[key2][(key-1)] = value2
			}
		}
	}

	//add sensor data
	for _, value:= range tempArray {
		if value[0]!="" && value[1]!="" && (value[0]!="n.a." || configuration.removeNAnames==false) && value[1]!="--" {
			offset, err := strconv.ParseFloat(value[2], 32)
			if err != nil {
				// do something sensible
			}
			gain, err := strconv.ParseFloat(value[3], 32)
			if err != nil {
				// do something sensible
			}
			rawvalue, err := strconv.ParseFloat(value[4],32)
			if err != nil {
				// do something sensible
			}
			humanValue:=float64(0)
			humanValueString:=""
			if value[0]=="Time" {
				timeValue:=int(rawvalue)
				minutes:=timeValue%256
				hours:=(timeValue-minutes)/256
				if hours<10 {
					humanValueString="0"
				}
				humanValueString+=strconv.Itoa(hours)+":"
				if minutes<10 {
					humanValueString+="0"
				}
				humanValueString+=strconv.Itoa(minutes)
				returnValue.Sysinfo.Time=humanValueString
				continue
			} else {
				humanValue=(rawvalue*gain)+offset;
				humanValueString=fmt.Sprintf("%.2f", humanValue)
			}

			if value[0]=="CPU Temp" {
				returnValue.Sysinfo.CPUTemp = humanValueString
			} else {
				minValue:=float64(0)
				maxValue:=float64(0)
				// add max/min
				if getStateData.Id!=0 {
					if getStateData.Sensors[len(returnValue.Actors)].MaxValue < humanValue {
						maxValue=humanValue
					} else {
						maxValue=getStateData.Sensors[len(returnValue.Actors)].MaxValue
					}

					if humanValue < getStateData.Sensors[len(returnValue.Actors)].MinValue  {
						minValue=humanValue
					} else {
						minValue=getStateData.Sensors[len(returnValue.Actors)].MinValue
					}
				}

				returnValue.Sensors = append(returnValue.Sensors, getStateSensors{value[0],value[1],offset,gain, rawvalue, humanValueString, maxValue, minValue})
			}

		}
	}

	//add actor data
	for _, value:= range tempArray {
		if value[1]=="--" && (value[0]!="n.a." || configuration.removeNAnames==false)  {
		   currentValue, _:= strconv.Atoi(value[4])
		   currentValueDescription:=""
		   switch currentValue {
		   		case 0: currentValueDescription="auto off"
		   		case 1: currentValueDescription="auto on"
		   		case 2: currentValueDescription="manual off"
		        case 3: currentValueDescription="manual on"
		       default:  currentValueDescription="unknown"
		   }

           returnValue.Actors = append(returnValue.Actors,getStateActors{value[0],currentValue, currentValueDescription})
		}
	}

	saveGetStateHistory(returnValue);
	getStateData = returnValue
	if configuration.pushStateURL!="" {
		tmpData := base64.StdEncoding.EncodeToString([]byte(returnValue.OriginalData))

		resp, err := http.PostForm(configuration.pushStateURL, url.Values{"secret": {configuration.shareSecret}, "data": {tmpData}})
		if err != nil {
			log.Print("Got an Push Error - pushStateURL correct?")
			log.Print(err)
			return
		}

		log.Print(resp)
	}
}

func getStateDaemon() {
	for {
		getStateConvert(getStateCSV())
		time.Sleep(configuration.pullIntervall * time.Second)
	}
}


func main()  {
	configInit()
	if configuration.enablePulling {
		if configuration.enableWebserver {
			go getStateDaemon()
		} else {
			getStateDaemon()
		}
	}

	if configuration.enableWebserver {
		startWebserver()
	}
}
