package main

import "time"

type getStateJSON struct {
	Id int64 `json:"id"`
	DataTime int64 `json:"time"`
	Sysinfo sysInfo `json:"sysinfo,omitempty"`
	Sensors []getStateSensors `json:"sensors,omitempty"`
	Actors []getStateActors `json:"actors,omitempty"`
	OriginalData string `json:"-"`
}

type sysInfo struct {
	Time string `json:"time,omitempty"`
	Uptime int `json:"uptime,omitempty"`
	CPUTemp string `json:"cpuTemp"`
	Info string `json:"sysinfoString,omitempty"`
}

type getStateSensors struct {
	Name string `json:"name"`
	Unit string `json:"unit"`
	Offset float64 `json:"offset"`
	Gain float64 `json:"gain"`
	RawValue float64 `json:"raw"`
	Value string `json:"value"`
	MaxValue float64 `json:"maxValue"`
	MinValue float64 `json:"minValue"`
}

type getStateActors struct {
    Name string `json:"name"`
    Status int `json:"status"`
    StatusDescription string `json:"statusDescription"`
}


type configurationStruct struct {
	enablePulling bool
	pullIntervall time.Duration
	getStateURL  string
	pushStateURL string
	removeNAnames bool
	enableWebserver bool
	shareSecret string
	allowHttpUpdate bool
	webserverBase string
	webserverRoot string
}