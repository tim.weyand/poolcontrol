FROM alpine:latest
ENV CURRENT_ROOT=/wwwroot/

RUN apk --no-cache add ca-certificates \
 && mkdir /wwwroot/

ADD go-control /
COPY wwwroot /wwwroot/

EXPOSE 80
CMD ["/go-control"]