# PoolDigital ProCon Proxy

Information:

This GoLang Helper is pulling Original Data from PoolDigital ProCon.IP primarily to not expose the Control directly to the Internet.

Also it offers:
* Data conversion to json
* Pushing Data to a second Helper Node (Private LAN => Webserver)


##Running

Environment Variables

| option                          | description                      |  Default Values |
| ---------------------------- | -------------------------------- | -------------------------------- | 
| enablePulling | Allow to retrieve Data from GetStateURL | true | 
| pullIntervall | seconds between retrieving Data | 3 | 
| getStateURL | URL to PoolDigital ProCon.IP | https://pool.weyand.biz/data/GetState.csv (demo) |
| removeNAnames | Remove Sesors/Actors with the name "n.a." | true |
| pushStateURL | URL to second ProCon Proxy | n/a |
| enableWebserver | enable the Webserver | true |
| webserverBase | if this docker is behind a reverse Proxy f.e. /data | true |
| allowHttpUpdate | allow Update via HTTP | true | 
| shareSecret | allows only receiving Data if the ShareSecret match Master/Slave | IamASecret129$ |

